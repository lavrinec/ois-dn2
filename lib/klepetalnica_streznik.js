var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var gesla=[];

exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj');
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajZahtevoZaZasebnoSporocilo(socket,uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    socket.on('gosti', function() {
      socket.emit('gosti', unk(socket,trenutniKanal[socket.id]));
    });
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}
function unk(socket,kanal){
  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 0) {
    var uporabnikiNaKanaluPovzetek="";
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
  }
  return uporabnikiNaKanaluPovzetek;
}

function pridruzitevKanalu(socket, kanal) {
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  socket.emit('pridruzitevOdgovor', {kanal: kanal});
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });

  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
  }
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}
function obdelajZahtevoZaZasebnoSporocilo(socket, uporabljeniVzdevki) {
  socket.on('zasebnoSporocilo', function(vzdevek,vsebina) {
    var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
    var uporabnikiNaKanalu = io.sockets.clients();
    
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1||prejsnjiVzdevek==vzdevek) {
        //console.log("Neuspesno.");
        socket.emit('napaka', {
          besedilo: 'Sporočila '+vsebina+' uporabniku z vzdevkom '+vzdevek+' ni bilo mogoče posredovati.'
        });
      } else {
        //console.log("Neuspesno 2.");
        
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevek);
        socket.emit('sporocilo', {
          besedilo: "(zasebno za "+vzdevek+"): "+vsebina
        });
        socket.broadcast.to(trenutniKanal[uporabnikiNaKanalu.indexOf(vzdevek).id]).emit('Zsporocilo', {
            za:vzdevek,
            besedilo: prejsnjiVzdevek+" (zasebno): "+vsebina
          });
      }
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
    });
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
    var uporabnikiNaKanalu = io.sockets.clients(kanal.novKanal);
    if(gesla[kanal.novKanal]==undefined||gesla[kanal.novKanal]==""||uporabnikiNaKanalu.length < 1){
      socket.leave(trenutniKanal[socket.id]);
      pridruzitevKanalu(socket, kanal.novKanal);
      gesla[kanal.novKanal]="";
    }else{
      socket.emit('sporocilo', {besedilo: 'Pridružitev v kanal '+kanal.novKanal+' ni bilo uspešno, ker je geslo napačno!'});
    }
  });
  socket.on('pridruzitevZahtevaGeslo', function(kanal) {
    var uporabnikiNaKanalu = io.sockets.clients(kanal.novKanal);
    var geslo=kanal.geslo;
    if(gesla[kanal.novKanal]==geslo){
      socket.leave(trenutniKanal[socket.id]);
      pridruzitevKanalu(socket, kanal.novKanal);
    }else  if(uporabnikiNaKanalu.length < 1){
      socket.leave(trenutniKanal[socket.id]);
      pridruzitevKanalu(socket, kanal.novKanal);
      gesla[kanal.novKanal]=geslo;
    }else  if(gesla[kanal.novKanal]==""){
      socket.emit('sporocilo', {besedilo: 'Izbrani kanal '+kanal.novKanal+' je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite z uporabo /pridruzitev '+kanal.novKanal+' ali zahtevajte kreiranje kanala z drugim imenom. '});
    }else{
      socket.emit('sporocilo', {besedilo: 'Pridružitev v kanal '+kanal.novKanal+' ni bilo uspešno, ker je geslo napačno!'});
    }
  });
}

function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
}