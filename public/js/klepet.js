var kletvice;
var datoteka = new XMLHttpRequest();
datoteka.open("GET", "swearWords.txt", true);
datoteka.onreadystatechange = function (){
    if(datoteka.readyState === 4){
        kletvice = datoteka.responseText.split('\n');
    }
}
datoteka.send();
var zvezdice=["*"];
for(var abc=1;abc<=15;abc++){
    zvezdice[abc]=zvezdice[abc-1]+"*";
}
var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};
Klepet.prototype.spremeniKanalGeslo = function(kanal, geslo) {
  this.socket.emit('pridruzitevZahtevaGeslo', {
    novKanal: kanal,
    geslo: geslo
  });
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      kanal = kanal.split("\"");
      if(kanal.length==5)
        this.spremeniKanalGeslo(kanal[1].substring(0, kanal[1].length), kanal[3].substring(0, kanal[3].length));
      else if(kanal.length==1)
        this.spremeniKanal(kanal);
      else
        sporocilo = 'Napaka v narekovajih!';
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      var besedilo = besede.join(' ');
      besede = besedilo.split("\"");
      var i=0;
      if(besede.length==5){
    var bes=" "+besede[3].substring(0, besede[3].length)+" ";
    for(i in kletvice) {
      var re = new RegExp('\\b'+kletvice[i]+'\\b', 'gi');
      bes=bes.replace(re, zvezdice[kletvice[i].length-1]);
    }
    var lt = /</g, 
    gt = />/g, 
    ap = /'/g, 
    ic = /"/g,
    bes=bes.replace(lt, "&lt;").replace(gt, "&gt;").replace(ap, "&#39;").replace(ic, "&#34;").
  replace(/;\)/g, "<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png\" alt=\";)\">").
  replace(/:\)/g, "<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png\" alt=\":)\">").
  replace(/\(y\)/g, "<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png\" alt=\"(y)\">").
  replace(/:\*/g, "<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png\" alt=\":*\">").
  replace(/:\(/g, "<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png\" alt=\":(\">");
      
        this.socket.emit('zasebnoSporocilo', besede[1].substring(0, besede[1].length), bes);
      }else{
        sporocilo = 'Sintakticna napaka izvedbe ukaza zasebno! Bodite pozorni na pravilno uporabo narekovajev.';
      }
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};