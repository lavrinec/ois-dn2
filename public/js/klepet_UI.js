var kletvice;
var datoteka = new XMLHttpRequest();
datoteka.open("GET", "swearWords.txt", true);
datoteka.onreadystatechange = function (){
    if(datoteka.readyState === 4){
        kletvice = datoteka.responseText.split('\n');
    }
}
datoteka.send();
var zvezdice=["*"];
for(var abc=1;abc<=15;abc++){
    zvezdice[abc]=zvezdice[abc-1]+"*";
}

function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').html(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    var i=0, j=0;
    var besede = sporocilo.split(' ');
    for(j in besede){
      for(i in kletvice) {
        if(kletvice[i].toLowerCase().localeCompare(besede[j].toLowerCase())==0){
          besede[j]=zvezdice[kletvice[i].length-1];
        }
      }
    }
    sporocilo=" "+besede.join(' ')+" ";
    i=0;
    for(i in kletvice) {
      var re = new RegExp('\\b'+kletvice[i]+'\\b', 'gi');
      sporocilo=sporocilo.replace(re, zvezdice[kletvice[i].length-1]);
    }
    var lt = /</g, 
    gt = />/g, 
    ap = /'/g, 
    ic = /"/g,
    sporocilo=sporocilo.replace(lt, "&lt;").replace(gt, "&gt;").replace(ap, "&#39;").replace(ic, "&#34;").
  replace(/;\)/g, "<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png\" alt=\";)\">").
  replace(/:\)/g, "<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png\" alt=\":)\">").
  replace(/\(y\)/g, "<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png\" alt=\"(y)\">").
  replace(/:\*/g, "<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png\" alt=\":*\">").
  replace(/:\(/g, "<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png\" alt=\":(\">");
    var km=$('#kanal').text().split(" @ ");
    klepetApp.posljiSporocilo(km[km.length-1], sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();
var ime,skupina;
var vzdevek;

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      vzdevek=rezultat.vzdevek;
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      ime=rezultat.vzdevek;
      $('#kanal').text(ime+" @ "+skupina);
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    skupina=rezultat.kanal;
    $('#kanal').text(ime+" @ "+skupina);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  socket.on('napaka', function(rezultat) {
    var sporocilo = rezultat.besedilo;
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').html(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });
  socket.on('Zsporocilo', function (sporocilo) {
    if(sporocilo.za==vzdevek){
      var novElement = $('<div style="font-weight: bold"></div>').html(sporocilo.besedilo);
      $('#sporocila').append(novElement);
  }});

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  socket.on('gosti', function(gosti) {
    $('#seznam-gostov').empty();
    gosti=gosti.split(", ");
    for(var abc=0;abc<gosti.length;abc++)
      $('#seznam-gostov').append(divElementEnostavniTekst(gosti[abc]));
  });

  setInterval(function() {
    socket.emit('kanali');
    socket.emit('gosti');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});